# Git reset git ignore

git rm -r --cached .

# Sample Wiki Markdown

## heading 2

_italic_

**Bold**

```json
{
	"json": "json"
}
```

```java
public class Main{
    public static void main(String[] args){
        // Java code
    }
}
```

**Table**

| Name          | Type    | Descriptions      | Example  |
|---------------|---------|-------------------|----------|
| name          | string  | name              | jx       |


| No. | Team Member     | Role                                                                                                                        |
|-----|-----------------|-----------------------------------------------------------------------------------------------------------------------------|
| 1   | CHIN JIA XIONG  | Game state logic, Concurrent logic of Worker class (Player)                                                                 |
| 2   | CHOR CHOON HENG | Game board UI to draw points and lines, Game logic threading and UI update integration, Game setting UI                     |
| 3   | WEE WEING HWEE  | Persistence storage (Binary file), Test and check for race condition, Logging                                               |
| 4   | HO YU XUAN      | Application UI for main menu, game over page and game log page, Generate random points logic, UI navigation and integration |
