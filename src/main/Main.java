package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import main.java.constants.AudioPaths;

import java.nio.file.Paths;

public class Main extends Application {

    public static MediaPlayer bgm;

    @Override
    public void start(Stage primaryStage) throws Exception {
        // initialize music media
        Media media = new Media(Paths.get(AudioPaths.getBgm()).toUri().toString());
        bgm = new MediaPlayer(media);
        bgm.play();

        primaryStage.setTitle("Game Menu");
        Pane root = FXMLLoader.load(getClass().getResource("/main/resources/view/MainMenu.fxml"));
        String css = this.getClass().getResource("/main/resources/style.css").toExternalForm();
        root.getStylesheets().add(css);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public static void main(String[] args) {
        Application.launch(Main.class);
    }

}
