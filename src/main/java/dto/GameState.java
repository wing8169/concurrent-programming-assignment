package main.java.dto;

import java.io.Serializable;

// GameState ...
public class GameState implements Serializable {
    private static final long serialVersionUID = 1L;

    private volatile boolean isRunning = true;

    public void setIsRunning(boolean isRunning) {
        this.isRunning = isRunning;
    }

    public boolean isRunning() {
        return isRunning;
    }
}
