package main.java.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;

// Activity ...
public class Activity implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String name;
    private String message;
    public Timestamp timestamp;

    public Activity(String name, String message) {
        id = UUID.randomUUID().toString();
        this.name = name;
        this.message = message;
        this.timestamp = new Timestamp(System.currentTimeMillis());
    }

    // toStringWithoutTimestamp returns activity string without timestamp
    public String toStringWithoutTimestamp() {
        return name + message;
    }

    @Override
    public String toString() {
        return String.format("%23s: [%9s] %s", timestamp.toString(), name, message);
    }
}
