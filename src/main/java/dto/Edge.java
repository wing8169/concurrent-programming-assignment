package main.java.dto;

import javafx.scene.paint.Color;

import java.io.Serializable;

// Edge ...
public class Edge implements Serializable {
    private static final long serialVersionUID = 1L;
    private Point point1, point2;
    private Color color;

    public Edge(Point point1, Point point2, Color color) {
        this.point1 = point1;
        this.point2 = point2;
        this.color = color;
    }

    public Point getPoint1() {
        return point1;
    }

    public Point getPoint2() {
        return point2;
    }

    public Color getColor() {
        return color;
    }

    @Override
    public String toString() {
        return point1.toString() + " : " + point2.toString();
    }
}
