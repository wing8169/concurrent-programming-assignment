package main.java.dto;

import java.io.Serializable;

// Point ...
public class Point implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private double x;
    private double y;
    private volatile boolean selected;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
        this.id = String.format("%.2f_%.2f", x, y);
        selected = false;
    }

    public String getId() {
        return id;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public boolean getSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String toString() {
        return String.format("[%.2f, %.2f]", x, y);
    }
}
