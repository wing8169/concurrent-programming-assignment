package main.java.dto;

import java.io.Serializable;

// Setting
public class Setting implements Serializable {
    private static final long serialVersionUID = 1L;
    private int n;
    private int t;
    private int m;
    private int s;

    public Setting(int n, int t, int m, int s) {
        this.n = n;
        this.t = t;
        this.m = m;
        this.s = s;
    }

    public int getN() {
        return n;
    }

    public int getT() {
        return t;
    }

    public int getM() {
        return m;
    }

    public int getS() {
        return s;
    }
}
