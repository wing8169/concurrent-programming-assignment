package main.java.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

// GameResult ...
public class GameResult implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private ArrayList<String> players;
    private Setting setting;
    private Date time;
    private long gameLength;

    public ArrayList<String> getPlayers() {
        return players;
    }

    public GameResult(ArrayList<String> players, Setting setting, Date time, long gameLength) {
        id = UUID.randomUUID().toString();
        this.players = players;
        this.setting = setting;
        this.time = time;
        this.gameLength = gameLength;
        processPlayersText();
    }

    // processPlayersText process players string into text with ranking and set to the array list
    private void processPlayersText(){
        for(int i=0; i<players.size(); i++) {
            String player = players.get(i);
            player = "Rank " + (i+1) + "," + player;
            players.set(i, player);
        }
    }
}
