package main.java.dto;

import javafx.scene.paint.Color;
import main.java.controller.GameBoardController;
import main.java.model.GameModel;
import main.java.model.SettingModel;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

// Player ...
public class Player implements Callable<Player>, Comparable<Player>, Serializable {
    private static final long serialVersionUID = 1L;
    private ConcurrentHashMap<String, Point> points;
    private LinkedList<Edge> edges;
    private int failureCount = 0;
    private GameState gameState;
    private GameModel gameModel;
    private Random random;
    private List<String> pointKeys;
    private String name;
    private Color color;
    private GameBoardController controller;
    private Setting setting;

    public Player(String name, ConcurrentHashMap<String, Point> points, GameState gameState, GameBoardController controller, GameModel gameModel) {
        edges = new LinkedList<>();
        random = new Random();
        this.points = points;
        this.gameState = gameState;
        this.name = name;
        this.controller = controller;
        this.gameModel = gameModel;
        SettingModel settingModel = new SettingModel();
        setting = settingModel.get();
        pointKeys = Collections.list(points.keys());
        color = Color.color(random.nextDouble(), random.nextDouble(), random.nextDouble(), 0.5);
    }

    public LinkedList<Edge> getEdges() {
        return edges;
    }

    public int getFailureCount() {
        return failureCount;
    }

    public String getName() {
        return name;
    }

    public void setFailureCount(int failureCount) {
        this.failureCount = failureCount;
    }

    // drawEdge generates random line, validate and draw to board if success
    private void drawEdge() {
        int index1 = random.nextInt(pointKeys.size());
        int index2 = random.nextInt(pointKeys.size());
        Point point1 = points.get(pointKeys.get(index1));
        Point point2 = points.get(pointKeys.get(index2));
        Edge edge = new Edge(point1, point2, color);
        boolean result = gameModel.drawEdge(this, edge);
        if (result) {
            edges.add(edge);
            failureCount = 0;
            if (controller != null) controller.drawLine(edge);
        }
    }

    @Override
    public Player call() throws Exception {
        while (gameState.isRunning()) {
            drawEdge();
            Thread.sleep(setting.getS());
        }
        return this;
    }

    @Override
    public String toString() {
        return name + ",Total edges - " + edges.size() + ",Total failures - " + failureCount + ",";
    }

    @Override
    public int compareTo(Player o) {
        // if lose, rank last regardless edge count
        if (failureCount >= 20) return 1;
        // total edges (more edge first) then total failures (less failure first)
        if (edges.size() > o.getEdges().size()) return -1;
        if (edges.size() < o.getEdges().size()) return 1;
        if (failureCount < o.getFailureCount()) return -1;
        if (failureCount > o.getFailureCount()) return 1;
        // by default alphabetical order
        return name.compareTo(o.getName());
    }
}
