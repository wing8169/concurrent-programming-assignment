package main.java.utility;

// ScreenUtils contain methods for screen processing
public class ScreenUtils {

    // getXPosition converts x coordinate to fit to screen width
    public static double getXPosition(double n) {
        return n / 1000.0 * 800.0;
    }

    // getYPosition converts y coordinate to fit to screen height
    public static double getYPosition(double n) {
        return n / 1000.0 * 600.0;
    }

}
