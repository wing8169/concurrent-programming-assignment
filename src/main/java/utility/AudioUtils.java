package main.java.utility;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import main.java.constants.AudioPaths;

import java.nio.file.Paths;

public class AudioUtils {

    private static MediaPlayer buttonClickAudio, buttonHoverAudio;

    public static synchronized void playButtonClick() {
        Media media = new Media(Paths.get(AudioPaths.getButtonClickAudio()).toUri().toString());
        buttonClickAudio = new MediaPlayer(media);
        buttonClickAudio.play();
    }

    public static synchronized void playButtonHover() {
        Media media = new Media(Paths.get(AudioPaths.getButtonHoverAudio()).toUri().toString());
        buttonHoverAudio = new MediaPlayer(media);
        buttonHoverAudio.play();
    }
}
