package main.java.utility;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Timestamp;

// Logger contains log method
public class Logger {

    // log logs the activities in log file
    public static synchronized void log(String message) {
        try {
            BufferedWriter bw;
            FileWriter fw;
            File f;
            String location = System.getProperty("user.dir") + "\\data\\logs\\" + java.time.LocalDate.now() + ".txt";
            f = new File(location);
            // create folder and file if does not exist
            f.getParentFile().mkdirs();
            f.createNewFile();
            if (f.exists() && !f.isDirectory()) {
                fw = new FileWriter(location, true);
            } else {
                fw = new FileWriter(location);
            }
            bw = new BufferedWriter(fw);
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            bw.write(timestamp + " : " + message);
            bw.newLine();
            bw.close();
        } catch (Exception ignored) {
        }
    }
}
