package main.java.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import main.java.dto.Activity;
import main.java.model.ActivityModel;
import main.java.utility.AudioUtils;

import java.util.LinkedList;

public class MainMenuController {
    ActivityModel am = new ActivityModel();
    @FXML
    private Line line;

    @FXML
    void exitClick(MouseEvent event) {
        Platform.exit();
        System.exit(0);
    }

    @FXML
    void logClick(MouseEvent event) {
        AudioUtils.playButtonClick();
        Platform.runLater(() -> {
            try {
                Pane GameLog = FXMLLoader.load(getClass().getResource("/main/resources/view/GameLog.fxml"));

                // Formatting text
                LinkedList<Activity> activities = am.getActivities();
                StringBuilder activitiesStr = new StringBuilder();
                activitiesStr.append("\n");
                for (Activity a : activities) {
                    activitiesStr.append("\t" + a.toString() + "\t");
                    activitiesStr.append("\n");
                }
                activitiesStr.append("\n");

                Text text = new Text();
                text.setFont(Font.font(null, FontWeight.BOLD, 14));
                text.setFill(Color.WHITE);
                text.setText(activitiesStr.toString());

                ScrollPane sp = new ScrollPane();
                sp.setTranslateX(25);
                sp.setTranslateY(65);

                sp.setContent(text);
                sp.setPrefSize(750, 450);
                sp.setStyle("-fx-background: rgba(0,0,0,0.3);\n -fx-background-color: rgba(0,0,0,0.3)");
                sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
                sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
                sp.setFitToHeight(true);
                VBox vbox = new VBox(sp);

                final Group root = new Group(GameLog, vbox);
                Stage stage;
                stage = new Stage();
                stage.setScene(new Scene(root, 800, 600));
                root.getStylesheets().add(this.getClass().getResource("/main/resources/style.css").toExternalForm());
                stage.setTitle("Game Log");
                stage.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @FXML
    void optClick(MouseEvent event) {
        AudioUtils.playButtonClick();
        try {
            Stage settingStage = new Stage();
            settingStage.setTitle("Setting");
            Pane gameBoard = FXMLLoader.load(getClass().getResource("/main/resources/view/Setting.fxml"));
            settingStage.setScene(new Scene(gameBoard));
            settingStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void startClick(MouseEvent event) {
        AudioUtils.playButtonClick();
        try {
            ((Stage) line.getScene().getWindow()).close();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/main/resources/view/GameBoard.fxml"));
            Parent root = (Parent) loader.load();
            GameBoardController gameBoardController = loader.<GameBoardController>getController();
            gameBoardController.init((Stage) line.getScene().getWindow());
            gameBoardController.startGame();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void creditClick(MouseEvent event) {
        AudioUtils.playButtonClick();
        try {
            Stage creditStage = new Stage();
            creditStage.setTitle("Credit");
            Pane credit = FXMLLoader.load(getClass().getResource("/main/resources/view/Credit.fxml"));
            creditStage.setScene(new Scene(credit));
            creditStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void mouseEntered() {
        AudioUtils.playButtonHover();
    }
}
