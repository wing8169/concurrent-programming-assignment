package main.java.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.java.dto.Setting;
import main.java.model.SettingModel;
import main.java.utility.AudioUtils;

import java.io.IOException;

public class SettingController {

    SettingModel settingModel = new SettingModel();

    @FXML
    private TextField inputN;
    @FXML
    private TextField inputT;
    @FXML
    private TextField inputM;
    @FXML
    private TextField inputS;

    @FXML
    private void initialize() {
        Setting setting = settingModel.get();
        inputN.setText(Integer.toString(setting.getN()));
        inputT.setText(Integer.toString(setting.getT()));
        inputM.setText(Integer.toString(setting.getM()));
        inputS.setText(Integer.toString(setting.getS()));
    }

    // validateSetting validates setting within given range
    private boolean validateSetting(Setting setting) {
        if (setting.getN() < 2 || setting.getN() > 200) return false;
        if (setting.getT() < 1 || setting.getT() > 20) return false;
        if (setting.getM() < 1 || setting.getM() > 3600) return false;
        if (setting.getS() < 100) return false;
        return true;
    }

    @FXML
    private void saveSetting() {
        AudioUtils.playButtonClick();
        int n = Integer.parseInt(inputN.getText());
        int t = Integer.parseInt(inputT.getText());
        int m = Integer.parseInt(inputM.getText());
        int s = Integer.parseInt(inputS.getText());

        Setting setting = new Setting(n, t, m, s);
        if (!validateSetting(setting)) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Error saving settings.");
            alert.setContentText("Please ensure that these conditions are achieved:\n" +
                    "n > 1 and <= 200\n" +
                    "t  > 1 and <= 20\n" +
                    "m > 1 and <= 3600\n" +
                    "s >= 100");
            alert.show();
            return;
        }

        try {
            ((Stage) inputM.getScene().getWindow()).close();
            settingModel.set(setting);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText("Successfully saved settings.");
            alert.setContentText("Settings have been successfully saved into the database.");
            alert.show();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Error saving settings.");
            alert.setContentText("Failed to save settings due to database error.");
            alert.show();
        }
    }

    @FXML
    void mouseEntered() {
        AudioUtils.playButtonHover();
    }
}
