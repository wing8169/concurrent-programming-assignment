package main.java.controller;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.Glow;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Sphere;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import main.java.constants.DefaultSetting;
import main.java.dto.Edge;
import main.java.dto.GameResult;
import main.java.dto.Point;
import main.java.dto.Setting;
import main.java.model.GameModel;
import main.java.model.SettingModel;
import main.java.utility.ScreenUtils;

// GameBoardController controls the GameBoard view
public class GameBoardController {
    public static Group group = new Group();
    GameModel gameModel = new GameModel();
    SettingModel settingModel = new SettingModel();
    Stage stage;

    public void init(Stage stage) {
        this.stage = stage;
    }

    // startGame starts a new game
    public void startGame() {
        Setting setting = settingModel.get();
        group = new Group();
        Scene scene = new Scene(group, DefaultSetting.getScreenWidth(), DefaultSetting.getScreenHeight(), Color.BLACK);
        stage = new Stage();
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();

        // new thread to start game logic
        new Thread(
                () -> {
                    gameModel.initModel(this, setting.getN(), setting.getT(), setting.getM());
                }
        ).start();
    }

    // gameOverScreen navigates to game over screen
    public void gameOverScreen(GameResult gameResult) {
        // get players texts ordered by ranking
        String players = gameResult.getPlayers().toString();

        // string processing to render player texts
        players = players.replaceAll(",, ", "\n\n");
        players = players.replaceAll(",", "\n");
        players = players.substring(1, players.length() - 1);
        System.out.println(players);
        final String playersConst = players;

        // navigate to game over screen
        Platform.runLater(() -> {
            try {
                Pane GameOver = FXMLLoader.load(getClass().getResource("/main/resources/view/GameOver.fxml"));

                // Formatting text
                Text text = new Text();
                text.setFont(Font.font(null, FontWeight.BOLD, 20));
                text.setFill(Color.GREY);
                text.setText(playersConst);
                text.setTranslateX(20);
                text.setTranslateY(20);

                ScrollPane sp = new ScrollPane();
                sp.setTranslateX(25);
                sp.setTranslateY(110);

                sp.setContent(text);
                sp.setPrefSize(230, 400);
                sp.setStyle("-fx-background: rgba(0,0,0,0.3);\n -fx-background-color: rgba(0,0,0,0.3);\n-fx-background-radius : 1.0em;\n");
                sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
                sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
                sp.setFitToHeight(true);

                VBox vbox = new VBox(sp);
                final Group root = new Group(GameOver, vbox);
                stage.setScene(new Scene(root, 800, 600));
                stage.setTitle("Game Over");
                root.getStylesheets().addAll(this.getClass().getResource("/main/resources/style.css").toExternalForm());
                stage.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    // drawSphere draws point
    public void drawSphere(Point point) {
        Sphere sphere = new Sphere();
        sphere.setRadius(5);
        sphere.setTranslateX(ScreenUtils.getXPosition(point.getX()));
        sphere.setTranslateY(ScreenUtils.getYPosition(point.getY()));
        Glow glow = new Glow();
        glow.setLevel(0.9);
        sphere.setEffect(glow);
        Platform.runLater(() -> {
            group.getChildren().add(sphere);
        });
    }

    // drawLine draws line
    public void drawLine(Edge edge) {
        Line line = new Line();
        line.setStartX(ScreenUtils.getXPosition(edge.getPoint1().getX()));
        line.setEndX(ScreenUtils.getXPosition(edge.getPoint1().getX()));
        line.setStartY(ScreenUtils.getYPosition(edge.getPoint1().getY()));
        line.setEndY(ScreenUtils.getYPosition(edge.getPoint1().getY()));
        line.setStroke(edge.getColor());
        Glow glow = new Glow();
        glow.setLevel(0.9);
        line.setEffect(glow);
        line.setStrokeWidth(2);
        Platform.runLater(() -> {
            group.getChildren().add(line);
            Timeline timeline = new Timeline(
                    new KeyFrame(
                            Duration.seconds(1),
                            new KeyValue(
                                    line.endXProperty(),
                                    ScreenUtils.getXPosition(edge.getPoint2().getX()),
                                    Interpolator.LINEAR
                            ),
                            new KeyValue(
                                    line.endYProperty(),
                                    ScreenUtils.getYPosition(edge.getPoint2().getY()),
                                    Interpolator.LINEAR
                            )
                    )
            );
            timeline.play();
        });
    }
}
