package main.java.controller;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import main.java.utility.AudioUtils;

// CreditController controls the Credit view
public class CreditController extends Application {

    @FXML
    private Button button;

    @FXML
    void gitLabLinkClick(ActionEvent event) {
        getHostServices().showDocument("https://gitlab.com/wing8169/concurrent-programming-assignment");
    }

    @FXML
    void wikiLinkClick(ActionEvent event) {
        getHostServices().showDocument("https://gitlab.com/wing8169/concurrent-programming-assignment/-/wikis/home");
    }

    @FXML
    void backMainMenu(ActionEvent event) {
        try {
            ((Stage) button.getScene().getWindow()).close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void mouseEntered() {
        AudioUtils.playButtonHover();
    }

    @Override
    public void start(Stage primaryStage) {
    }
}
