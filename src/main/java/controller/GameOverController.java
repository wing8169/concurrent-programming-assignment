package main.java.controller;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import main.java.dto.Activity;
import main.java.model.ActivityModel;
import main.java.utility.AudioUtils;

import java.util.LinkedList;

public class GameOverController {
    Stage stage;
    ActivityModel am = new ActivityModel();

    @FXML
    ImageView rotateImage;

    @FXML
    ImageView rotateImage2;

    @FXML
    ImageView rotateText;

    Timeline rotateT = new Timeline();
    Timeline rotate = new Timeline();
    Timeline rotate2 = new Timeline();

    @FXML
    protected void initialize() {
        startAnimations();
    }

    // rotateImage rotates image once (can set to INDEFINITE for indefinite cycle)
    public void rotateImage() {
        DoubleProperty r = rotateImage.rotateProperty();

        rotate.getKeyFrames().addAll(
                new KeyFrame(new Duration(0), new KeyValue(r, 0)),
                new KeyFrame(new Duration(5000), new KeyValue(r, -100))
        );
        rotate.setCycleCount(1);
        rotate.play();
    }

    // rotateImage2 rotates image once (can set to INDEFINITE for indefinite cycle)
    public void rotateImage2() {
        DoubleProperty r = rotateImage2.rotateProperty();

        rotate2.getKeyFrames().addAll(
                new KeyFrame(new Duration(0), new KeyValue(r, 0)),
                new KeyFrame(new Duration(5000), new KeyValue(r, 100))
        );
        rotate2.setCycleCount(1);
        rotate2.play();
    }

    // rotateT rotates text once (can set to INDEFINITE for indefinite cycle)
    public void rotateT() {
        DoubleProperty r = rotateText.rotateProperty();

        rotateT.getKeyFrames().addAll(
                new KeyFrame(new Duration(0), new KeyValue(r, 0)),
                new KeyFrame(new Duration(5000), new KeyValue(r, 70))
        );
        rotateT.setCycleCount(1);
        rotateT.play();
    }

    // startAnimations starts all animations
    public void startAnimations() {
        rotateImage();
        rotateImage2();
        rotateT();
    }

    // shutdownAnimations stop all animations
    public void shutdownAnimations() {
        rotate.stop();
        rotate2.stop();
        rotateT.stop();
    }

    @FXML
    void ClickMain(MouseEvent event) {
        Platform.runLater(() -> {
            try {
                shutdownAnimations();
                ((Stage) rotateImage.getScene().getWindow()).close();
                stage = new Stage();
                stage.setTitle("Game Menu");
                Pane mainMenu = FXMLLoader.load(getClass().getResource("/main/resources/view/MainMenu.fxml"));
                mainMenu.getStylesheets().add(this.getClass().getResource("/main/resources/style.css").toExternalForm());
                stage.setScene(new Scene(mainMenu));
                stage.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    @FXML
    void ClickGameLog(MouseEvent event) {
        AudioUtils.playButtonClick();
        Platform.runLater(() -> {
            try {
                Pane GameLog = FXMLLoader.load(getClass().getResource("/main/resources/view/GameLog.fxml"));

                // Formatting text
                LinkedList<Activity> activities = am.getActivities();
                StringBuilder activitiesStr = new StringBuilder();
                for (Activity a : activities) {
                    activitiesStr.append(a.toString()).append("\t");
                    activitiesStr.append("\n");
                }
                activitiesStr.append("\n");

                Text text = new Text();
                text.setFont(Font.font(null, FontWeight.BOLD, 14));
                text.setFill(Color.WHITE);
                text.setTranslateX(20);
                text.setTranslateY(20);
                text.setText(activitiesStr.toString());

                ScrollPane sp = new ScrollPane();
                sp.setTranslateX(25);
                sp.setTranslateY(65);

                sp.setContent(text);
                sp.setPrefSize(750, 450);
                sp.setStyle("-fx-background: rgba(0,0,0,0.3);\n -fx-background-color: rgba(0,0,0,0.3)");
                sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
                sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
                sp.setFitToHeight(true);
                VBox vbox = new VBox(sp);

                final Group root = new Group(GameLog, vbox);

                stage = new Stage();
                stage.setScene(new Scene(root, 800, 600));
                root.getStylesheets().add(this.getClass().getResource("/main/resources/style.css").toExternalForm());
                stage.setTitle("Game Log");
                stage.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @FXML
    void mouseEntered() {
        AudioUtils.playButtonHover();
    }
}
