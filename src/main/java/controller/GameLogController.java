package main.java.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import main.java.utility.AudioUtils;

// GameLogController controls the GameLog view
public class GameLogController {
    @FXML
    private Button backBtn;

    @FXML
    void BackMain(MouseEvent event) {
        try {
            ((Stage) backBtn.getScene().getWindow()).close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void mouseEntered() {
        AudioUtils.playButtonHover();
    }
}
