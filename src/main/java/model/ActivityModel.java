package main.java.model;

import main.java.dto.Activity;
import main.java.utility.ByteUtils;
import main.java.utility.Logger;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.util.LinkedList;

// ActivityModel ...
public class ActivityModel {
    private final String location = System.getProperty("user.dir") + "\\data\\activities.dat";
    private final LinkedList<Activity> activities = new LinkedList<>();

    // createFileIfNotFound creates folder and file if does not exist
    private void createFileIfNotFound() {
        try {
            File file = new File(location);
            // create folder and file if does not exist
            file.getParentFile().mkdirs();
            file.createNewFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // save saves the activities into dat file
    public void save() {
        createFileIfNotFound();
        try {
            FileOutputStream fos = new FileOutputStream(location);
            fos.write(ByteUtils.objectToBytes(activities));
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // appendActivity appends activity into activities list
    public synchronized void appendActivity(Activity activity) {
        String message = activity.toStringWithoutTimestamp();
        Logger.log(message);
        activities.add(activity);
        System.out.println(message);
    }

    // getActivities return all activities in dat file
    public LinkedList<Activity> getActivities() {
        createFileIfNotFound();
        File file = new File(location);
        byte[] bArray = ByteUtils.fileToByteArray(file);
        try (ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bArray))) {
            @SuppressWarnings("unchecked")
            LinkedList<Activity> activities = (LinkedList<Activity>) ois.readObject();
            ois.close();
            return activities;
        } catch (Exception e) {
            return new LinkedList<>();
        }
    }
}