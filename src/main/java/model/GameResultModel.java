package main.java.model;

import main.java.dto.GameResult;
import main.java.utility.ByteUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.util.LinkedList;

// GameResultModel ...
public class GameResultModel {

    private final String location = System.getProperty("user.dir") + "\\data\\results.dat";

    private void createFileIfNotFound() {
        try {
            File file = new File(location);
            // create folder and file if does not exist
            file.getParentFile().mkdirs();
            file.createNewFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void create(GameResult gameResult) {
        createFileIfNotFound();
        try {
            LinkedList<GameResult> results = getAll();
            results.add(gameResult);
            FileOutputStream fos = new FileOutputStream(location);
            fos.write(ByteUtils.objectToBytes(results));
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public LinkedList<GameResult> getAll() {
        createFileIfNotFound();
        File file = new File(location);
        byte[] bArray = ByteUtils.fileToByteArray(file);
        try (ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bArray))) {
            @SuppressWarnings("unchecked")
            LinkedList<GameResult> results = (LinkedList<GameResult>) ois.readObject();
            return results;
        } catch (Exception e) {
            return new LinkedList<>();
        }
    }

}
