package main.java.model;

import main.java.controller.GameBoardController;
import main.java.dto.*;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

// GameModel ...
public class GameModel {
    GameBoardController controller;
    GameResultModel gameResultModel;
    ActivityModel activityModel = new ActivityModel();
    SettingModel settingModel = new SettingModel();
    GameState gameState = new GameState();

    // initModel initializes points, starts players threads with timeout and navigates to player class
    public void initModel(GameBoardController controller, int n, int t, int m) {
        long gameStart = System.currentTimeMillis();
        Random random = new Random();
        gameResultModel = new GameResultModel();
        ConcurrentHashMap<String, Point> points = new ConcurrentHashMap<>();
        this.controller = controller;
        while (points.size() < n) {
            int i = random.nextInt(100001);
            int j = random.nextInt(100001);
            double x = (double) i / 100f;
            double y = (double) j / 100f;

            // if points already exist in map, do not keep the point
            if (points.containsKey(String.format("%.2f_%.2f", x, y))) continue;

            // initialize point
            Point point = new Point(x, y);
            points.put(point.getId(), point);
            if (controller != null) {
                controller.drawSphere(point);
            }
        }
        ArrayList<Player> players = new ArrayList<>();
        ExecutorService executorService = Executors.newFixedThreadPool(t);
        for (int i = 0; i < t; i++) {
            Player player = new Player("Player " + i, points, gameState, controller, this);
            players.add(player);
        }
        try {
            executorService.invokeAll(players, m, TimeUnit.SECONDS);
        } catch (Exception ignored) {
        }

        executorService.shutdown();
        activityModel.appendActivity(new Activity("System", " Game Over"));

        // sort the player by ranking
        Collections.sort(players);

        // convert players to list of string
        ArrayList<String> playersStr = new ArrayList<>();
        for (Player player : players) {
            playersStr.add(player.toString());
        }

        // get setting
        Setting setting = settingModel.get();

        // compute game length
        long gameLength = setting.getM() * 1000 < System.currentTimeMillis() - gameStart ?
                setting.getM() * 1000 : System.currentTimeMillis() - gameStart;

        // init game result
        GameResult gameResult = new GameResult(playersStr, setting, new Date(), gameLength);

        System.out.println(players);
        activityModel.save();
        gameResultModel.create(gameResult);

        // go to game over screen after waiting for 1 second to render all graphics
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ignored) {
        }

        if (controller != null) controller.gameOverScreen(gameResult);
    }

    // drawEdge validates the edge and return true if success, else false
    public synchronized boolean drawEdge(Player player, Edge edge) {
        // abort the operation if gameState is false
        if (!gameState.isRunning()) return false;

        if (edge.getPoint1().getId().equals(edge.getPoint2().getId()) ||
                edge.getPoint1().getSelected() ||
                edge.getPoint2().getSelected()) {
            activityModel.appendActivity(new Activity(player.getName(), " => The dot is already selected. Failure count: " +
                    (player.getFailureCount() + 1) +
                    ". Point 1: " + edge.getPoint1() + " Point 2: " + edge.getPoint2()));
            player.setFailureCount(player.getFailureCount() + 1);
            if (player.getFailureCount() >= 20 && gameState.isRunning()) {
                activityModel.appendActivity(new Activity(player.getName(), " => lose the game."));
                gameState.setIsRunning(false);
            }
            return false;
        }
        edge.getPoint1().setSelected(true);
        edge.getPoint2().setSelected(true);
        activityModel.appendActivity(new Activity(player.getName(), " => " + edge));
        return true;
    }

    // main is driver method for testing
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int t = scanner.nextInt();
        int m = scanner.nextInt();
        GameModel model = new GameModel();
        model.initModel(null, n, t, m);
    }
}
