package main.java.model;

import main.java.constants.DefaultSetting;
import main.java.dto.Setting;
import main.java.utility.ByteUtils;

import java.io.*;

// SettingModel ...
public class SettingModel {

    private final String location = System.getProperty("user.dir") + "\\data\\settings.dat";

    private void createFileIfNotFound() {
        try {
            File file = new File(location);
            // create folder and file if does not exist
            file.getParentFile().mkdirs();
            file.createNewFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void set(Setting setting) throws IOException {
        createFileIfNotFound();
        // write to the file
        FileOutputStream fos = new FileOutputStream(location);
        fos.write(ByteUtils.objectToBytes(setting));
        fos.close();
    }

    public Setting get() {
        createFileIfNotFound();
        File file = new File(location);
        byte[] bArray = ByteUtils.fileToByteArray(file);
        try (ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bArray))) {
            Setting setting = (Setting) ois.readObject();
            ois.close();
            return setting;
        } catch (Exception e) {
            return new Setting(
                    DefaultSetting.getDefaultN(),
                    DefaultSetting.getDefaultT(),
                    DefaultSetting.getDefaultM(),
                    DefaultSetting.getDefaultS()
            );
        }
    }

}
