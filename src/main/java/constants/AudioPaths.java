package main.java.constants;

public class AudioPaths {
    private static final String bgm = "src\\main\\resources\\audio\\bgm.mp3";
    private static final String buttonClickAudio = "src\\main\\resources\\audio\\buttonClick.mp3";
    private static final String buttonHoverAudio = "src\\main\\resources\\audio\\buttonHover.mp3";

    public static String getBgm() {
        return bgm;
    }

    public static String getButtonClickAudio() {
        return buttonClickAudio;
    }

    public static String getButtonHoverAudio() {
        return buttonHoverAudio;
    }
}
