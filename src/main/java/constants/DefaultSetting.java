package main.java.constants;

// DefaultSetting defines the default settings of program
public class DefaultSetting {
    // game setting
    private static final int DEFAULT_N = 200; // number of points
    private static final int DEFAULT_T = 20; // number of players
    private static final int DEFAULT_M = 3600; // time limit in seconds
    private static final int DEFAULT_S = 500; // speed of players in milliseconds

    // screen setting
    private static final int SCREEN_WIDTH = 800; // screen width in px
    private static final int SCREEN_HEIGHT = 600; // screen height in px

    public static int getScreenWidth() {
        return SCREEN_WIDTH;
    }

    public static int getScreenHeight() {
        return SCREEN_HEIGHT;
    }

    public static int getDefaultN() {
        return DEFAULT_N;
    }

    public static int getDefaultT() {
        return DEFAULT_T;
    }

    public static int getDefaultM() {
        return DEFAULT_M;
    }

    public static int getDefaultS() {
        return DEFAULT_S;
    }
}
